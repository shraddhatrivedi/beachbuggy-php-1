<?php
require '/home/admin/web/default.domain/public_html/beachbuggy/api/autoload.php';
include('/home/admin/web/default.domain/public_html/beachbuggy/api/config.php');
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;
//include('/home/admin/web/default.domain/public_html/beachbuggy/api/push_client.php');
ParseClient::initialize( $app_id, $rest_api,$master_key);
$data = json_encode(array("action:key"=>1,"alert" => 'The administrator will review your submission and you will be notified if your registration is verified.',"sound" => "default"));

// Push to Channels
/*ParsePush::send(array(
  "channels" => ["driver"],
  "data" => $data
));*/

// Push to Query
$query = ParseInstallation::query();
//$query->equalTo('deviceType', 'android');
$query->equalTo('emailID',$emailID);
ParsePush::send(array(
  "where" => $query,
  "data" => $data
));


?>
