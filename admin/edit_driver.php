<?php
include('includes/conn.php');
 include('includes/header.php');
		$driverid = $_GET['driverid'];
$sql="select * from tbldriver WHERE `pkDriverId` = $driverid ";
$result = mysqli_query($con,$sql);
$row = mysqli_fetch_array($result);

//print_r($row);
if(!isset($_SESSION['admin_id'])){
	header('Location: login.php');
	exit();
}

 ?>
 <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Driver</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Edit Driver Information
							<a  href="driver.php" class="btn btn-primary btn-xs" style="float:right" >Back</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" name="edit_user" method="post" action="submit_driver.php" enctype="multipart/form-data" onsubmit="return user_validation();">
										<input type="hidden" name="edit" id="edit" value="edit"/>
                                         <span id="error"> * is Required Field </span>
										<input type="hidden" name="driverid" id="driverid" value="<?php echo $row['pkDriverId'];  ?>"/>
                                        <div class="form-group">
                                            <label>First Name</label> <span id="errorstar">*</span>   
                                            <input class="form-control" placeholder="Enter Your First Name" name="fname" id="fname" value="<?php echo $row['firstName']; ?>" required>
											</div>
                                        <div class="form-group">
                                            <label>Last Name</label> <span id="errorstar">*</span>   
                                            <input class="form-control" name="lname" id="lname" placeholder="Enter Your First Name" value="<?php echo $row['lastName']; ?>" required>
											</div>
                                        
                                        <div class="form-group">
                                            <label>Email ID</label> <span id="errorstar">*</span>   
                                            <input class="form-control" name="email" id="username" placeholder="Enter Your emailID" value="<?php echo $row['emailID']; ?>" required>
                                        </div>
										<div class="form-group">
                                            <label>Address </label> <span id="errorstar">*</span>   
                                            <textarea class="form-control" rows="3" name="address" id="address" required><?php echo $row['homeAddress']; ?></textarea>
                                        </div>
										
										<div class="form-group">
                                            <label>Phone No </label> <span id="errorstar">*</span>   
                                            <input class="form-control" name="ph_no" id="ph_no" placeholder="Enter Your Phone number" maxlength="10" value="<?php echo $row['phoneNo']; ?>" required>
                                        </div>
										
                                        <div class="form-group">
                                            <label>License No </label> <span id="errorstar">*</span>   
                                            <input class="form-control" name="license_no" id="license_no" placeholder="Enter Your license number" value="<?php echo $row['licenseNumber']; ?>" required>
                                        </div>
										
										
										<div class="form-group">
                                            <label>Queue Size </label> <span id="errorstar">*</span>   
                                            <input class="form-control" name="queue_size" id="queue_size" placeholder="Enter queue size" value="<?php echo $row['queueSize']; ?>" required>
                                        </div>
										<input type="hidden" name="cur_status" value="<?php echo $row['status']; ?>"/>
										<div class="form-group">
                                            <label> Status : <label>
                                            <label class="radio-inline">
                                                <input type="radio" name="status" id="optionsRadiosInline1" value="UA" <?php echo ($row['status']=='UA') ?  "checked" : ""; ?> >UA
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="status" id="optionsRadiosInline2" value="A" <?php echo($row['status']=='A') ?  "checked" : ""; ?>>A
                                            </label> 
											
                                             
                                        </div>
										
                                        <button type="submit" class="btn btn-success">Submit Button</button>
                                        <button type="reset" class="btn btn-warning">Reset Button</button>
                                    </form>
                                </div>
                               
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Forms -->

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Forms - Use for reference -->

</body>

</html>
