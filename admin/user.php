<?php 
include('includes/conn.php');
if(!isset($_SESSION['admin_id'])){
	header('Location: login.php');
	exit();
}
include('includes/header.php');
$sql="select * from tblUser";
$result = mysqli_query($con,$sql);
//$row = mysqli_fetch_array($result);

//print_r($row);


 ?>


        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">User</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            User Data Table
							<!--<a  href="user_add.php" class="btn btn-primary btn-xs" style="float:right" >Add User</a>-->
                        </div>
						
							
						
						
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone Number</th>
                                            <th class="photocs">Image</th>
											<th style="text-align:center">Action</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
									
									<?php
									//print_r($result);
									while($row = mysqli_fetch_array($result)){?>
                                        <tr class="odd gradeX">
                                            
                                            <td><?php echo $row['fname']." ". $row['lname']; ?></td>
                                            <td><?php echo $row['email'] ; ?></td>
                                            <td class="center"><?php echo $row['phone_no']; ?></td>
                                            <td class="center photocs">
												<?php if($row['image'] == ''){?>
											 	<img src="upload/default_user.jpg" width="150" height="100"/>
											 	 <?php }
												 	else{
												  ?>
											 	<span class="gallery"><a href="upload/user/big/<?php echo $row['image']; ?>" rel="prettyPhoto" title="<?php echo $row['fname'].' '.$row['lname'];?>"><img src="<?php echo "upload/user/small/" . $row['image']; ?>"  alt="<?php echo $row['fname'].' '.$row['lname']; ?>" /></a></span>
											<?php } ?>
											</td>
											<td align="center"><a href="user_edit.php?id=<?php echo $row['uid'];   ?>" class="btn btn-info btn-circle"><i class="fa fa-pencil"></i></a>
															   <a href="user_submit.php?id=<?php echo $row['uid'];?>"  class="btn btn-warning btn-circle"><i class="fa fa-times"></i></a>
											</td>
                                            
                                        </tr>
										<?php } ?>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
      
    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
	<link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
	<script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" charset="utf-8">
	$(document).ready(function(){
	  $("a[rel^='prettyPhoto']").prettyPhoto();
	});
	</script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Tables -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
    </script>

</body>

</html>
