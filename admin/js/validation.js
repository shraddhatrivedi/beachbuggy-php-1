function feedback_validation()
	{
		var regexp = /^[a-zA-Z]+$/;
		
		/* ***************** feedback title ********************** */
		var feedback_title = document.getElementById('feedback_title').value;
		
		if(feedback_title=="" || feedback_title=="NULL")
		{
			 document.getElementById('error').innerHTML ="feedback about must be filled out";
			return false;
		}
		
		if(!(feedback_title.match(regexp)))
		{
			document.getElementById('error').innerHTML ="feedback about must be filled out only character or letters";
			return false;
		}
		
		/* ***************** Name ********************** */
		var name = document.getElementById('name').value;
		
		if(name=="" || name=="NULL")
		{
			document.getElementById('error').innerHTML = "Your Name must be filled out";
			return false;
		}
		
		if(!(name.match(regexp)))
		{
			document.getElementById('error').innerHTML = "Your name must be filled out only character or letters";
			return false;
		}
		
		
		/* ***************** Phone number ********************** */
		var ph_no = document.getElementById('phone').value;
		
		if(ph_no=="" || ph_no=="NULL")
		{
			document.getElementById('error').innerHTML ="Phone number must be filled out";
			return false;
		}
		
		if(isNaN(ph_no))
		{
			document.getElementById('error').innerHTML ="Please fill only numeric value in phone number";
			return false;
		}
		/* ***************** email ********************** */
		var email = document.getElementById('email').value;
		if(email=="" || email=="NULL")
		{
			document.getElementById('error').innerHTML ="Email Address must be filled out";
			return false;
		}
		
		 	var atpos = email.indexOf("@");
			var dotpos = email.lastIndexOf(".");
			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
			{
				document.getElementById('error').innerHTML ="Not a valid Email address";
				return false;
			}
	}

/************************************** Gallery   ******************************************************/

function gallery_validation ()
	{
	
		/* ***************** title ********************** */
		var title = document.getElementById('title').value;
		
		if(title=="" || title=="NULL")
		{
			 document.getElementById('error').innerHTML ="Title must be filled out";
			return false;
		}
		
		/* ***************** Category ********************** */
		var category = document.getElementById('category').value;
		
		if(category=="0")
		{
			document.getElementById('error').innerHTML ="Please Select Category";
			return false;
		}
	
	}
	
/************************************** Donation   ******************************************************/	
function donation_validation ()
	{
	
		/* ***************** Name ********************** */
		var name = document.getElementById('name').value;
		
		if(name=="" || name=="NULL")
		{
			 document.getElementById('error').innerHTML ="Donar's Name must be filled out";
			return false;
		}
		
		/* ***************** Amount ********************** */
		var amount = document.getElementById('amount').value;
		
		if(amount=="" || amount=="NULL")
		{
			 document.getElementById('error').innerHTML ="Amount must be filled out";
			return false;
		}
		
		if(isNaN(amount))
		{
			document.getElementById('error').innerHTML ="Please fill only numeric value in Amount";
			return false;
		}
		
		
			
	}
	
/************************************** News   ******************************************************/	
function news_validation ()
	{
	
		/* ***************** news title ********************** */
		var news_title = document.getElementById('news_title').value;
		
		if(news_title=="" || news_title=="NULL")
		{
			 document.getElementById('error').innerHTML ="News title must be filled out";
			return false;
		}
		
			
	
	}
	
/************************************** News comment   ******************************************************/	
function newscomment_validation ()
	{
	
		/* ***************** Comment ********************** */
		var Comment = document.getElementById('Comment').value;
		
		if(Comment=="" || Comment=="NULL")
		{
			 document.getElementById('error').innerHTML ="Comment must be filled out";
			return false;
		}
		
			
	
	}
	
/************************************** Project   ******************************************************/	
function event_validation ()
	{
	
		/* ***************** Event name ********************** */
		var event_name = document.getElementById('event_name').value;
		
		if(event_name=="" || event_name=="NULL")
		{
			 document.getElementById('error').innerHTML ="Event Name must be filled out";
			return false;
		}
		
		/* ***************** event venue ****************** */
		var event_venue = document.getElementById('event_venue').value;
		
		if(event_venue=="" || event_venue=="NULL")
		{
			 document.getElementById('error').innerHTML ="Event Venue must be filled out";
			return false;
		}	
		
		
		
		/* ***************** Date Time ********************** */
		var datetime = document.getElementById('datetime').value;
		
		if(datetime=="" || datetime=="NULL")
		{
			 document.getElementById('error').innerHTML ="Date and Time must be filled out";
			return false;
		}
		
		
		/* *****************Event Organizer ********************** */
		var regexp = /^[a-zA-Z]+$/;
		
		var organizer = document.getElementById('organizer').value;
		
		if(organizer=="" || organizer=="NULL")
		{
			 document.getElementById('error').innerHTML ="Event Organizer must be filled out";
			return false;
		}
		
		if(!(organizer.match(regexp)))
		{
			document.getElementById('error').innerHTML ="Event Organizer name must be filled out only character or letters";
			return false;
		}
	
	
	
	}
	
	/* ***************** user ********************** */
	function user_validation()
	{
		var regexp = /^[a-zA-Z]+$/;
		
		/* ***************** First Name ********************** */
		var fname = document.getElementById('fname').value;
		
		if(fname=="" || fname=="NULL")
		{
			 document.getElementById('error').innerHTML ="First name must be filled out";
			return false;
		}
		
		if(!(fname.match(regexp)))
		{
			document.getElementById('error').innerHTML ="First name must be filled out only character or letters";
			return false;
		}
		
		/* ***************** last Name ********************** */
		var lname = document.getElementById('lname').value;
		
		if(lname=="" || lname=="NULL")
		{
			document.getElementById('error').innerHTML = "Last name must be filled out";
			return false;
		}
		
		if(!(lname.match(regexp)))
		{
			document.getElementById('error').innerHTML = "Last name must be filled out only character or letters";
			return false;
		}
		
		/* ***************** User Name ********************** */
		var uname = document.getElementById('username').value;
		
		if(uname=="" || uname=="NULL")
		{
			document.getElementById('error').innerHTML = "Username must be filled out";
			return false;
		}
		
		if(uname.length < 5) {
        	document.getElementById('error').innerHTML =" Username must contain at least five characters!";
        	return false;
		}
		
		
		/* ***************** password ********************** */
		var pass = document.getElementById('password').value;
		
		if(pass=="" || pass=="NULL")
		{
			document.getElementById('error').innerHTML ="Password must be filled out";
			return false;
		}
		
		if(pass.length < 6)
		{
			document.getElementById('error').innerHTML ="Password must contain at least six characters!";
			return false;
		}
		
		/* ***************** Phone number ********************** */
		var ph_no = document.getElementById('ph_no').value;
		
		if(ph_no=="" || ph_no=="NULL")
		{
			document.getElementById('error').innerHTML ="Phone number must be filled out";
			return false;
		}
		
		if(isNaN(ph_no))
		{
			document.getElementById('error').innerHTML ="Please fill only numeric value in phone number";
			return false;
		}
		
		/* ***************** country ********************** */
		var country = document.getElementById('country').value;
		
		if(country=="0")
		{
			document.getElementById('error').innerHTML ="Please Select Country";
			return false;
		}
		
		
		/* ***************** state ********************** */
		var state = document.getElementById('state').value;
		
		if(state=="0")
		{
			document.getElementById('error').innerHTML ="Please Select State";
			return false;
		}
		
		/* ***************** city ********************** */
		var city = document.getElementById('city').value;
		
		if(city =="" || city =="NULL")
		{
			document.getElementById('error').innerHTML ="City must be filled out";
			return false;
		}
		
		
		/* ***************** User Type ********************** */
		var user_type = document.getElementById('user_type').value;
		
		if(user_type=="0")
		{
			document.getElementById('error').innerHTML ="Please Select User Type";
			return false;
		}
	}

/************************************** Category   ******************************************************/

function category_validation ()
	{
	
		/* ***************** category Name ********************** */
		var categoryName = document.getElementById('categoryName').value;
		
		if(categoryName=="" || categoryName=="NULL")
		{
			 document.getElementById('error').innerHTML ="category Name must be filled out";
			return false;
		}
		
				
	}
	
/************************************** Award   ******************************************************/

function award_validation ()
	{
	
		/* ***************** Award Title ********************** */
		var award_title = document.getElementById('award_title').value;
		
		if(award_title=="" || award_title=="NULL")
		{
			 document.getElementById('error').innerHTML ="Award Title must be filled out";
			return false;
		}
		
		
		/* ***************** Date Time ********************** */
		var datetime = document.getElementById('datetime').value;
		
		if(datetime=="" || datetime=="NULL")
		{
			 document.getElementById('error').innerHTML ="Date and Time must be filled out";
			return false;
		}
		
				
	}
	
/************************************** Committee   ******************************************************/

function committee_validation ()
	{
	
		/* ***************** Committee Title ********************** */
		var committee_title = document.getElementById('committee_title').value;
		
		if(committee_title=="" || committee_title=="NULL")
		{
			 document.getElementById('error').innerHTML ="Committee Title must be filled out";
			return false;
		}
		
		
		/* ***************** Category ********************** */
		var category = document.getElementById('category').value;
		
		if(category=="0")
		{
			document.getElementById('error').innerHTML ="Please Select Category";
			return false;
		}
		
				
	}
	
