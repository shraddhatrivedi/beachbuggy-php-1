<?php 
include('includes/conn.php');
include('includes/header.php');
$sql="select * from driver_ad";
$result = mysqli_query($con,$sql);
$row = mysqli_fetch_array($result);


$f_sql="select * from rider_ad where `rider_image_cat` = 'footer'";
$f_result = mysqli_query($con,$f_sql);
$f_row = mysqli_fetch_array($f_result);


$m_sql="select * from rider_ad where `rider_image_cat` = 'middle'";
$m_result = mysqli_query($con,$m_sql);
$m_row = mysqli_fetch_array($m_result);
//print_r($row);

if(!isset($_SESSION['admin_id'])){
	header('Location: login.php');
	exit();
}
 ?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
 <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Driver Ad</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Adding Advertise images For Driver
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form action="upload.php" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="driver_footer" value="test">
										<div class="form-group" id="errorstar">Upload Image size 800x110</div>
										<div class="form-group">
                                            <img src="http://104.43.162.143/beachbuggy/admin/advertisement/<?php echo $row['driver_footer_image'];?>"/>
                                        </div>
										<div class="form-group">
                                            <label>For Driver Upload Image</label><span id="errorstar">*</span>
                                            <span class="btn btn-default btn-file">
												<input type="file" name="dr_footer_img">
											</span>
                                        </div>
										<div class="form-group">
										  <label for="exampleInputName">Link:</label>
										  <input type="url" name="link" class="form-control" placeholder="Enter Link" required value="<?php echo $row['driver_footer_link'];?>">
										</div>
                                        <button type="submit" class="btn btn-success">Upload</button>
                                    </form>
                                </div>
                               
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
			
			<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Rider Ad</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
			
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Adding Advertise images For Rider Footer
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form action="upload.php" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="rider_footer" value="test">
										<div class="form-group" id="errorstar">Upload Image size 800x110</div>
										<div class="form-group">
                                            <img src="http://104.43.162.143/beachbuggy/admin/advertisement/<?php echo $f_row['rider_image_name'];?>"/>
                                        </div>
										<div class="form-group">
                                            <label>For Rider Upload Image</label><span id="errorstar">*</span>
                                            <span class="btn btn-default btn-file">
												<input type="file" name="dr_footer_img">
											</span>
                                        </div>
										<div class="form-group">
										  <label for="exampleInputName">Link:</label>
										  <input type="url" name="link" class="form-control" placeholder="Enter Link" required value="<?php echo $f_row['rider_image_link'];?>"/>
										</div>
                                        <button type="submit" class="btn btn-success">Upload</button>
                                    </form>
                                </div>
                               
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
			
			
			<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Adding Advertise images For Rider Middle
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form action="upload.php" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="rider_middle" value="test">
										<div class="form-group" id="errorstar">Upload Image size 800x360</div>
										<div class="form-group">
                                            <img src="http://104.43.162.143/beachbuggy/admin/advertisement/<?php echo $m_row['rider_image_name'];?>"/>
                                        </div>
										<div class="form-group">
                                            <label>For Rider Upload Image</label><span id="errorstar">*</span>
                                            <span class="btn btn-default btn-file">
												<input type="file" name="dr_footer_img">
											</span>
                                        </div>
										<div class="form-group">
										  <label for="exampleInputName">Link:</label>
										  <input type="url" name="link" class="form-control" placeholder="Enter Link" required value="<?php echo $m_row['rider_image_link'];?>"/>
										</div>
                                        <button type="submit" class="btn btn-success">Upload</button>
                                    </form>
                                </div>
                               
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
			
			
			
			
			
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Forms -->

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Forms - Use for reference -->

</body>

</html>
