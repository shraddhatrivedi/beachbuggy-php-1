<?php
include('includes/conn.php');
include('includes/header.php');
//$row = mysqli_fetch_array($result);
if(!isset($_SESSION['admin_id'])){
	header('Location: login.php');
	exit();
}
//print_r($row);


 ?>
<!--<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">-->
<!--<link href="http://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">-->
<link rel="stylesheet" type="text/css" media="all" href="css/daterangepicker-bs3.css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<!--<script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>-->
<script type="text/javascript" src="js/moment.js"></script>
<script type="text/javascript" src="js/daterangepicker.js"></script>
<script type="text/javascript">
var $j = jQuery.noConflict();
               $j(document).ready(function() {
                  $j('#dropup').daterangepicker({
                      drops:'up',
					  format: 'YYYY-MM-DD'
                  });
				 $j('#dropup').on('change', function() {
					//alert( this.value ); 
});
               });
               </script>
<style>
input[readonly] {
  cursor: pointer !important;
}
</style>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Pickup</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
			
			<!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Select Date for view pickup
							<!--<a  href="seat.php" class="btn btn-primary btn-xs" style="float:right" >Back</a>-->
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
<?php
/******get location name **********/
			function getaddress($lat,$lng)
			{
			$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false';
			$json = @file_get_contents($url);
			$data=json_decode($json);
			$status = $data->status;
			if($status=="OK")
			return $data->results[0]->formatted_address;	
			else
			return false;
			}
			/******end get location name **********/
if(isset($_POST['pickup']))
{
//$location_id = $_POST['location_id'];
	$date = $_POST['dropup'];
	$arr = explode(" - ", $date);
	$date_1 = $arr[0];
	$date_2 = $arr[1];

}
else
{
$date_2 = date("Y-m-d");
//$date_2 = date("Y-m-d");
$date_input = strtotime($date_2);
$date_input = strtotime("-7 day",$date_input);
$date_1 = date("Y-m-d",$date_input);
}
//$sql="select * from tblPickUp where DATE_FORMAT(pickupTime, '%Y-%m-%d') = '$date' ORDER BY `PkPickUpId` DESC";
$sql = "SELECT * FROM tblPickUp WHERE DATE_FORMAT(pickupTime, '%Y-%m-%d') >= '$date_1' AND DATE_FORMAT(pickupTime, '%Y-%m-%d') <= '$date_2' ORDER BY `PkPickUpId` DESC";
//echo $sql;
$result = mysqli_query($con,$sql);
?>
                                    <form role="form" name="add_user" method="post" action="">
										<input type="hidden" name="pickup" id="pickup" value="pickup"/>
                                        <div class="form-group">
                                            <label> Select Date : </label>
											<br/><br/>
                                           <input type="text" style="width: 200px" name="dropup" id="dropup" class="form-control" value="<?php echo $date_1.' - '.$date_2;?>" readonly />
                                        </div>
                                        <button type="submit" class="btn btn-success" >Submit</button>
                                        <!--<button type="reset" class="btn btn-warning">Reset Button</button>-->
                                    </form>
                                </div>
                               
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
			
			
			
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Pickup Data Table
							<!--<a  href="page_add.php" class="btn btn-primary btn-xs" style="float:right" >Add Pages</a>-->
                        </div>
						
						
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Pickup ID</th>
                                            <th>Pickup Time</th>
											<th>Pickup Location</th>
											<th>Destination Location</th>
											<th>Number Of Riders</th>
											<th>Driver Notes</th>
											<th>Rider Name</th>
											<th>Driver Name</th>
											<th>Live Status</th>
                                           <!--<th style="text-align:center">Action</th>-->
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
									
									<?php
									//print_r($result);
									while($row = mysqli_fetch_array($result)){
									//print_r($row);
									$u_sql = "select * from `tblUser` where  pkUserID = ".$row['fkUserID'];
									$u_res = mysqli_query($con,$u_sql);
									$u_row = mysqli_fetch_array($u_res);
									$user_name = $u_row['firstName'].' '.$u_row['lastName'];
									
									
									$d_sql = "select * from `tbldriver` where  pkDriverId = ".$row['fkDriverId'];
									$d_res = mysqli_query($con,$d_sql);
									$d_row = mysqli_fetch_array($d_res);
									$driver_name = $d_row['firstName'].' '.$d_row['lastName'];
									$pickup_name = getaddress($row['sourceLat'],$row['sourceLong']);
									$destination_name = getaddress($row['destinationLat'],$row['destinationLong']);
									if($row['status'] == 0)
									{
										$status = "Open";
									}
									else if($row['status'] == 1)
									{
										$status = "In Driver QUEUE";
									}
									else if($row['status'] == 2)
									{
										$status = "arrived";
									}
									else if($row['status'] == 3)
									{
										$status = "cancel by user";
									}
									else if($row['status'] == 5)
									{
										$status = "finish";
									}
									else if($row['status'] == 6)
									{
										$status = "No show";
									}
									else if($row['status'] == 7)
									{
										$status = "Late";
									}
									else if($row['status'] == 8)
									{
										$status = "System Cancelled";
									}
									else if($row['status'] == 9)
									{
										$status = "Driver Cancelled";
									}
									else if($row['status'] == 10)
									{
										$status = "Waiting";
									}
									else if($row['status'] == 11)
									{
										$status = "start";
									}
									else
									{
										$status = "";
									}
									?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $row['PkPickUpId']; ?></td>
                                            <td><?php echo $row['pickupTime']; ?></td>
											<td><?php echo $pickup_name ; ?></td>
											<td><?php echo $destination_name ; ?></td>
											<td><?php echo $row['numberOfRiders'] ; ?></td>
											<td><?php echo $row['driverNotes'] ; ?></td>
											<td><?php echo $user_name; ?></td>
											<td><?php echo $driver_name; ?></td>
											<td><?php echo $status; ?></td>
											<!--<td align="center">
												<a href="page_edit.php?pid=<?php echo $row['PkPickUpId'];   ?>" class="btn btn-info btn-circle"><i class="fa fa-pencil"></i></a>
												<a href="page_submit.php?pid=<?php echo $row['PkPickUpId'];?>"  class="btn btn-warning btn-circle"><i class="fa fa-times"></i></a>
											</td>-->
                                            
                                        </tr>
										<?php } ?>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
      
    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Tables -->
    <script src="js/plugins/dataTables/jquery.dataTables.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable({
		 "iDisplayLength": 25,
		 "aaSorting": [[0, 'desc']]
		});
    });
    </script>

</body>

</html>
