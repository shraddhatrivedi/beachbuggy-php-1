<?php 
include('includes/conn.php');
include('includes/header.php');
$sql="select * from user";
$result = mysqli_query($con,$sql);
//$row = mysqli_fetch_array($result);

//print_r($row);

if(!isset($_SESSION['admin_id'])){
	header('Location: login.php');
	exit();
}
 ?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
 <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add Popular Places</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Adding Popular Places Information
							<a  href="places.php" class="btn btn-primary btn-xs" style="float:right" >Back</a>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" method="post" action="submit_places.php">
										<input type="hidden" name="add" id="add" value="add"/>
                                        <span id="error"> * is Required Field </span>
                                        <div class="form-group">
                                            <label>Name</label>  <span id="errorstar">*</span>
                                            <input class="form-control" placeholder="Enter Name" name="name" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Latitude</label>  <span id="errorstar">*</span>
                                            <input class="form-control" name="lat" placeholder="Enter Latitude" required>
                                        </div>
										<div class="form-group">
                                            <label>Longitude</label>  <span id="errorstar">*</span>
                                            <input class="form-control" name="long" placeholder="Enter longitude" required>
                                        </div>
										
										<div class="form-group">
                                            <label> Status : <label>
                                            <label class="radio-inline">
                                                <input type="radio" name="status" value="1" checked="checked">Yes
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="status" value="0">No
                                            </label>
                                        </div>

                                        <button type="submit" class="btn btn-success" >Submit</button>
                                        <!--<button type="reset" class="btn btn-warning">Reset Button</button>-->
                                    </form>
                                </div>
                               
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Page-Level Plugin Scripts - Forms -->

    <!-- SB Admin Scripts - Include with every page -->
    <script src="js/sb-admin.js"></script>

    <!-- Page-Level Demo Scripts - Forms - Use for reference -->

</body>

</html>
